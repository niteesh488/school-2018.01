from ..keyvalue import KeyValueStorage

from pytest import raises


root = "/tmp/test"

def test_empty_rootdir():
    with raises(Exception):
        KeyValueStorage(rootdir="")
        KeyValueStorage()

def test_rootdir_init():
    err = "failed init for {} root dir".format(root)
    assert KeyValueStorage(rootdir=root), err

def test_does_not_exist(store=None, key="test"):
    store = store or KeyValueStorage(rootdir=root)
    assert store.exists(key) is False, key
    assert store.load(key) is None, key

def test_persist_and_remove():
    store = KeyValueStorage(rootdir=root)
    key = "ß"
    value = "¢¤¦ª"

    assert store.persist(key, value) is None
    with raises(Exception):
        test_does_not_exist(store, key)
    assert store.load(key) == value
    assert store.remove(key) == None
    test_does_not_exist(store, key)

def test_multiple_stores():
    a, b = "A", "B"
    storeA, storeB = KeyValueStorage(rootdir=root), KeyValueStorage(rootdir=root)

    def subtest_exists(key):
        with raises(Exception):
            test_does_not_exist(storeA, key)
            test_does_not_exist(storeB, key)

    def subtest_does_not_exist(stores, keys):
        for store in stores:
            for key in keys:
                test_does_not_exist(store, key)

    subtest_does_not_exist([storeA, storeB], [a, b])

    storeA.persist(a, b)
    subtest_exists(a)

    storeB.persist(b, a)
    subtest_exists(b)

    for store in [storeA, storeB]:
        for key in [a, b]:
            store.remove(key)

    subtest_does_not_exist([storeA, storeB], [a, b])


def test_iteration():
    store = KeyValueStorage(rootdir=root)
    xs = set(map(str, range(100,1000)))

    for x in xs:
        store.persist(x, x)

    keys = set(xs)
    for key in xs:
        with raises(Exception):
            test_does_not_exist(store, k)
        keys -= {key}
    assert len(keys) == 0

    keys = set(xs)
    for key in store.keys():
        keys -= {key}
    assert len(keys) == 0

    keys = set(xs)
    for key, value in store.items():
        assert key == value, "{}: {}".format(key, value)
        keys -= {key}
    assert len(keys) == 0

    for key in store:
        assert key in xs, key
        assert store.exists(key), key
        assert store.load(key) == key, key

    for key in store:
        assert key in xs
        store.remove(key)

    assert sum([print(key) for key in store]) == 0

    for key in xs:
        test_does_not_exist(store, key)
