from .logger import LOGGER
from .server import MasterService, ProxyService

from argparse import ArgumentParser
import json
import Pyro4


def main():
    p = ArgumentParser(description="Master server")
    p.add_argument(
        "--config",
        help="Path to config.json file",
        default="config.json")
    args = p.parse_args()

    with open(args.config) as config_file:
        config = json.load(config_file)

    Pyro4.config.SERIALIZER = config["serializer"]
    Pyro4.config.SERIALIZERS_ACCEPTED.add(config["serializer"])

    master_service = MasterService()
    proxy_service = ProxyService(config)

    daemon = Pyro4.Daemon(host=config["host"], port=config["port"])
    master_uri = daemon.register(master_service, "node")
    proxy_uri = daemon.register(proxy_service, "proxy")

    LOGGER.info("Master URI = {}".format(master_node_uri))
    LOGGER.info("Proxy URI = {}".format(master_uri_proxy))

    daemon.requestLoop()

if __name__ == "__main__":
    main()
