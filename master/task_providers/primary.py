import string
from itertools import product

from ..logger import LOGGER

MAX_PAGE_COUNT = 35


class PrimaryTaskProvider:

    def __init__(self):
        self._chars = [c for c in (string.ascii_lowercase + string.printable[:10])]
        self._query_len = 1
        self._tasks = self._get_task()

    def _get_task(self):
        options_list = product(self._chars, repeat=self._query_len)
        for query in options_list:
            query = "".join(map(str, query))
            for page in range(1, MAX_PAGE_COUNT):
                LOGGER.info("Coppying: %s" % query, page)
                yield (query, page)
        self._query_len += 1

    def get_node_task(self):
        try:
            return next(self._tasks)
        except (TypeError, StopIteration):
            self._tasks = self._get_task()
            return self.get_node_task()
